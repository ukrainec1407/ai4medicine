﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AIMedicine
{
    public partial class MainPage : ContentPage
    {
        internal int GetPuls()
        {
            var random = new Random();
            return random.Next(75, 81);
        }

        private int state = 0;
        internal void ChandeMainTeme(int helthGrade)
        {
            switch (helthGrade)
            {
                case 0:
                    BackGrounGr.Color = Color.FromHex("#1d86d1");
                    AnimalFace.Source = ImageSource.FromResource("AIMedicine.img.fineCat.png");
                    LabelBottom.Text = "You're good to go!";
                    break;
                case 1:
                    BackGrounGr.Color = Color.Orange;
                    AnimalFace.Source = ImageSource.FromResource("AIMedicine.img.normalCat.png");
                    LabelBottom.Text = "Hope you're feeling fine! If not please let me now ";
                    break;
                case 2:
                    BackGrounGr.Color = Color.Red;
                    AnimalFace.Source = ImageSource.FromResource("AIMedicine.img.sickCat.png");
                    LabelBottom.Text = "Maybe you should take it easy today and rest";
                    break;
            }
        }

        public MainPage()
        {
            InitializeComponent();
            int grade = 0;
            //AnimalFace.Source = ImageSource.FromResource("AIMedicine.img.fineCat.png");
            ChandeMainTeme(grade);
            Device.StartTimer(TimeSpan.FromSeconds(3), () =>
            {
                Model.refreshPuls();
                return true;
            });
        }
        private void AnimalFace_Clicked(object sender, EventArgs e)
        {
            //DisplayAlert("Alert", "Here is an advice", "OK");
            ChandeMainTeme(state);
            state++;
            if (state > 2)
                state=0;
        }

    }
}
