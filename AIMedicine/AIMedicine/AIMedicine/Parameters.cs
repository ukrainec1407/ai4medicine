﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIMedicine
{
    public class Parameters
    {
        public int id { get; set; }
        public string parameter { get; set; }
        public int value { get; set; }
        public string img { get; set; }
    }
}
