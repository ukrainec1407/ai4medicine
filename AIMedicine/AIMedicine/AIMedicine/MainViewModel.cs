﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace AIMedicine
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Parameters> Parameters;
        public ObservableCollection<Parameters> parameters {
            get { return Parameters; }
            set
            {
                Parameters = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("parameters"));
            }
        }

        public MainViewModel()
        {
            parameters = new ObservableCollection<Parameters>();
            addData();
        }

        public int getPuls()
        {
            var rnd = new Random();
            return rnd.Next(75, 82);
        }

        public void refreshPuls()
        {
            parameters[0].value = getPuls();
        }

        private void addData()
        {
            parameters.Add(new Parameters
            {
                id = 0,
                parameter = "bpm",
                value = getPuls(),
                img = "https://cdn0.iconfinder.com/data/icons/phosphor-fill-vol-3/256/heartbeat-fill-512.png"
            });
            parameters.Add(new Parameters
            {
                id = 1,
                parameter = "steps",
                value = 2780,
                img = "https://cdn0.iconfinder.com/data/icons/octicons/1024/steps-512.png"
            });
            parameters.Add(new Parameters
            {
                id = 2,
                parameter = "hours",
                value = 7,
                img = "https://cdn0.iconfinder.com/data/icons/user-interface-2063/24/UI_Essential_icon_expanded_2-13-512.png"
            });
        }
    }
}
